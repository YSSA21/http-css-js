'use strict';

//Ejercicio 11
//Escribe una función que, al recibir un array como parámetro, elimine los strings repetidos del mismo. No se permite hacer uso de Set ni Array.from().

const names = [
    'A-Jay',
    'Manuel',
    'Manuel',
    'Eddie',
    'A-Jay',
    'Su',
    'Reean',
    'Manuel',
    'A-Jay',
    'Zacharie',
    'Zacharie',
    'Tyra',
    'Rishi',
    'Arun',
    'Kenton',
];

const borraRepetidos = (array) => {
    const newNames = [];

    array.forEach((elemento) => {
        if (!newNames.includes(elemento)) {
            newNames.push(elemento);
        }
    });

    return newNames;
};

console.log(borraRepetidos(names));
