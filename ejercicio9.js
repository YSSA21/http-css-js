'use strict';

//Ejercicio 9
//Crea un programa que imprima cada 5 segundos el tiempo desde la ejecución del mismo. Formatea el tiempo para que se muestren los segundos, los minutos, las horas y los días desde la ejecución.

function muestraReloj() {
    const momentoActual = new Date();

    console.log(momentoActual);

    let horas = momentoActual.getHours();
    let minutos = momentoActual.getMinutes();
    let segundos = momentoActual.getSeconds();

    if (horas < 10) {
        horas = '0' + horas;
    }
    if (minutos < 10) {
        minutos = '0' + minutos;
    }
    if (segundos < 10) {
        segundos = '0' + segundos;
    }

    console.log('Son las ' + horas + ':' + minutos + ':' + segundos);
}
window.onload = function () {
    setInterval(muestraReloj, 5000);
};
