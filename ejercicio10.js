'use strict';

//Ejercicio 10
//Crea un programa que reciba un número en decimal o binario y devuelva la conversión:

//-   Si le pasamos un nº en decimal debe retornar la conversión a binario.
//-   Si le pasamos un nº en binario deberá devolver su equivalente decimal.
//Para ello la función deberá recibir un segundo parámetro que indique la base: 10 (decimal) o 2 (binario).
//No se permite utilizar el método parseInt().

function convertir(num1, base) {
    if (isNaN(num1)) {
        alert('Lo tecleado no es un número');
        return;
    }

    if (base === 2) {
        const binario = num1.toString(2);
        alert('el binario equivalente es ' + binario);
    } else if (base === 10) {
        const decimal = num1.toString(10);
        alert('el decimal equivalente es ' + decimal);
    } else {
        alert('La base entrada no corresponde a decimal ni binario');
    }
}
console.log(convertir(+prompt('Escribe un número'), +prompt('Escriba 2 si desea convertir a binario o 10 a decimal')));
